// eslint-disable-next-line import/no-extraneous-dependencies
const { semanticVersioning } = require('@jakec-dev/developer-tools');

const commitizenAdapter = semanticVersioning.createCommitizenAdapter({
  scopes: ['homepage', 'components', 'logic'],
});

module.exports = commitizenAdapter;

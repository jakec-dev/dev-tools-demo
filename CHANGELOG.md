## [1.0.2](https://gitlab.com/jakec-dev/dev-tools-demo/compare/v1.0.1...v1.0.2) (2024-03-24)

## [1.0.1](https://gitlab.com/jakec-dev/dev-tools-demo/compare/v1.0.0...v1.0.1) (2024-03-24)


### Bug Fixes

* **components:** TESTING ([142f4bb](https://gitlab.com/jakec-dev/dev-tools-demo/commit/142f4bb69d447cd91c1206d12f67a680b3d1dfe5))

# 1.0.0 (2024-03-24)


### Features

* **homepage:** [[AWR-333](https://localsearch.atlassian.net/browse/AWR-333)] Change heading text ([32664d8](https://gitlab.com/jakec-dev/dev-tools-demo/commit/32664d894410ab6e7915d1ec3a2a301da72d0507))

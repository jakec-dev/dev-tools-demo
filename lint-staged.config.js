import { lintStaged as createLintStagedConfig } from '@jakec-dev/developer-tools';

export default createLintStagedConfig({ tsc: false });

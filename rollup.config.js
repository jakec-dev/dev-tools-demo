import { readFile } from 'fs/promises';
import commonjs from '@rollup/plugin-commonjs';
import terser from '@rollup/plugin-terser';

const packageJson = JSON.parse(await readFile(new URL('./package.json', import.meta.url)));

export default {
  input: `src/index.js`,
  output: [
    {
      file: packageJson.module,
      format: 'es',
    },
    {
      file: packageJson.main,
      format: 'cjs',
    },
  ],
  plugins: [commonjs(), terser()],
};
